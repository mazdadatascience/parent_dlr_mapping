import pandas as pd
import os
from sqlalchemy import create_engine
from sqlalchemy.types import DateTime, Float, Integer, String

## create sql connection
os.environ["NLS_LANG"] = ".AL32UTF8"

engine = create_engine(
    "oracle+cx_oracle://RPR_STG:rpSgdEv12@x4ebid10.mnao.net:1521/EDWHDEV",
    max_identifier_length=128,
)

dlr_master = pd.read_sql("SELECT DLR_CD FROM EDW_STG.BTC02010_DEALER_MASTER", engine)


def get_top_parent(x):
    top_parent_query = """\
WITH CTE (DLR_CD, DBA_NM, NXT_DLR_CD, LVL) AS (
-- This is end of the recursion: Select items with no parent
	SELECT DLR_CD, DBA_NM, NXT_DLR_CD, 1 AS LVL
	FROM EDW_STG.BTC02010_DEALER_MASTER
	WHERE DLR_CD = :to_find
UNION ALL
-- This is the recursive part: It joins to CTE
	SELECT bdm.DLR_CD, bdm.DBA_NM, bdm.NXT_DLR_CD, LVL+1 AS LVL
	FROM EDW_STG.BTC02010_DEALER_MASTER bdm
	INNER JOIN CTE c ON bdm.DLR_CD = c.NXT_DLR_CD
)

SELECT * FROM CTE ORDER BY LVL DESC FETCH FIRST 1 ROW ONLY
"""
    parent_dlr = pd.read_sql(
        top_parent_query, engine, params={"to_find": x.get("dlr_cd")}
    )
    return pd.Series(
        [parent_dlr.iloc[0]["dlr_cd"], parent_dlr.iloc[0]["dba_nm"]],
        index=["parent_dlr", "parent_dlr_nm"],
    )


dlr_master[["parent_dlr", "parent_dlr_nm"]] = dlr_master.apply(get_top_parent, axis=1)

#temp to add
# to_add = pd.DataFrame({"dlr_cd": ["61643", "61642"], "parent_dlr": ["61643", "61642"], "parent_dlr_nm": ["SUBURBAN MAZDA OF FARMINGTON H", "SUBURBAN MAZDA OF TROY"]})

# dlr_master = pd.concat([dlr_master, to_add], ignore_index=True)
# dlr_master.loc[dlr_master["parent_dlr"] == "61639", "parent_dlr"] = "61643"
# dlr_master.loc[dlr_master["parent_dlr"] == "61638", "parent_dlr"] = "61642"


dlr_master.drop_duplicates().to_sql(
    "wp_parent_dealer_mapping",
    engine,
    if_exists="replace",
    index=False,
    dtype={"dlr_cd": String(5), "parent_dlr": String(5), "parent_dlr_nm": String(30)},
)
